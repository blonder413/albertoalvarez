<?php
use kartik\mpdf\Pdf;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$mailer = require __DIR__ . '/mailer.php';

$config = [
    'id' => 'alvertoalvarez',
    'name'  => 'AlvertoAlvarez',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language'  => 'es',
    'timeZone'          => 'America/Bogota',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'bgQFDBs_cxmcIdC0-WXtG5aPh144rnL-',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => $mailer,
        // https://demos.krajee.com/mpdf
        // setup Krajee Pdf component
        'pdf' => [
            'class'             => Pdf::classname(),
            'format'            => Pdf::FORMAT_LETTER,
            'mode'              => Pdf::MODE_CORE, 
            'orientation'       => Pdf::ORIENT_PORTRAIT,
            'destination'       => Pdf::DEST_BROWSER,
//            'cssFile'           => 'css/azul/pdf.css',
            'defaultFont'       => 'Roboto',
//            'marginTop'         => 13,
//            'marginBottom'      => 13,
//            'marginLeft'        => 13,
//            'marginRight'       => 13,
            'options'   => [
                'showWatermarkText' => true,
                'showWatermarkImage' => true,
            ],
            
            'defaultFontSize'   => 12,
            // refer settings section for all configuration options
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        
    ],
    'modules'   => [
        'gridview' =>  [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to  
            // use your own export download action or custom translation 
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ]
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
        'generators' => [ // HERE
            'crud' => [
                'class' => 'yii\gii\generators\crud\Generator',
                'templates' => [
                    'yii2-adminlte3' => '@vendor/hail812/yii2-adminlte3/src/gii/generators/crud/default', // template name => path to template
                    'myCrud' => '@app/myTemplates/crud/default',
                ]
            ]
        ],
    ];
}

return $config;
