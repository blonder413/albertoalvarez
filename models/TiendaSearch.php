<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tienda;

/**
 * TiendaSearch represents the model behind the search form of `app\models\Tienda`.
 */
class TiendaSearch extends Tienda
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'usuario_crea', 'usuario_modifica'], 'integer'],
            [['nombre', 'geolocalizacion', 'fecha_apertura', 'fecha_crea', 'fecha_modifica'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tienda::find();

        // add conditions that should always apply here
        
//        $query->joinWith(['usuarioCrea as creado_por' => function ($q) {
//            $q->andFilterWhere(['=', 'creado_por.username', $this->usuarioCrea]);
//        }]);
        
//        $query->joinWith(['usuarioModifica as actualizado_por' => function ($q) {
//            $q->andFilterWhere(['=', 'actualizado_por.username', $this->usuarioModifica]);
//        }]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'fecha_apertura' => $this->fecha_apertura,
            'usuario_crea' => $this->usuario_crea,
            'fecha_crea' => $this->fecha_crea,
            'usuario_modifica' => $this->usuario_modifica,
            'fecha_modifica' => $this->fecha_modifica,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'geolocalizacion', $this->geolocalizacion]);
        
//        $query->andFilterWhere(['like', 'creado_por.name', $this->usuario_crea])
//               ->andFilterWhere(['like', 'actualizado_por.name', $this->usuario_modifica]);

        return $dataProvider;
    }
}
