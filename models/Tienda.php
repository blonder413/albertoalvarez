<?php

namespace app\models;

use app\models\MiActiveRecord;
use app\models\User;
use Yii;

/**
 * This is the model class for table "tienda".
 *
 * @property int $id
 * @property string $nombre
 * @property string $geolocalizacion
 * @property string|null $fecha_apertura
 * @property int $usuario_crea
 * @property string $fecha_crea
 * @property int $usuario_modifica
 * @property string $fecha_modifica
 *
 * @property User $usuarioCrea
 * @property User $usuarioModifica
 */
class Tienda extends MiActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tienda';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'geolocalizacion', 'fecha_apertura'], 'required'],
            [['fecha_apertura', 'fecha_crea', 'fecha_modifica'], 'safe'],
            [['usuario_crea', 'usuario_modifica'], 'integer'],
            [['nombre', 'geolocalizacion'], 'string', 'max' => 255],
            [['nombre'], 'unique'],
            [['geolocalizacion'], 'unique'],
            [['usuario_crea'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['usuario_crea' => 'id']],
            [['usuario_modifica'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['usuario_modifica' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'geolocalizacion' => 'Geolocalizacion',
            'fecha_apertura' => 'Fecha Apertura',
            'usuario_crea' => 'Usuario Crea',
            'fecha_crea' => 'Fecha Crea',
            'usuario_modifica' => 'Usuario Modifica',
            'fecha_modifica' => 'Fecha Modifica',
        ];
    }

    /**
     * Gets query for [[UsuarioCrea]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioCrea()
    {
        return $this->hasOne(User::class, ['id' => 'usuario_crea']);
    }

    /**
     * Gets query for [[UsuarioModifica]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioModifica()
    {
        return $this->hasOne(User::class, ['id' => 'usuario_modifica']);
    }
}
