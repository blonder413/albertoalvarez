<?php

namespace app\models;

use app\models\MiActiveRecord;
use Yii;

/**
 * This is the model class for table "producto".
 *
 * @property int $id
 * @property string $nombre
 * @property string $sku
 * @property string|null $descripcion
 * @property int $valor
 * @property int $tienda_id
 * @property int $usuario_crea
 * @property string $fecha_crea
 * @property int $usuario_modifica
 * @property string $fecha_modifica
 *
 * @property Tienda $tienda
 * @property User $usuarioCrea
 * @property User $usuarioModifica
 */
class Producto extends MiActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'producto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'sku', 'valor', 'tienda_id'], 'required'],
            [['descripcion'], 'string'],
            [['valor', 'tienda_id', 'usuario_crea', 'usuario_modifica'], 'integer'],
            [['recibido'], 'boolean'],
            [['fecha_crea', 'fecha_modifica'], 'safe'],
            [['nombre', 'sku'], 'string', 'max' => 255],
            [['nombre'], 'unique'],
            [['sku'], 'unique'],
            [['recibido'], 'default', 'value' => false],
            [['tienda_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tienda::className(), 'targetAttribute' => ['tienda_id' => 'id']],
            [['usuario_crea'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['usuario_crea' => 'id']],
            [['usuario_modifica'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['usuario_modifica' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'sku' => 'Sku',
            'descripcion' => 'Descripción',
            'valor' => 'Valor',
            'tienda_id' => 'Tienda',
            'recibido'  => 'Recibido',
            'usuario_crea' => 'Usuario Crea',
            'fecha_crea' => 'Fecha Crea',
            'usuario_modifica' => 'Usuario Modifica',
            'fecha_modifica' => 'Fecha Modifica',
        ];
    }

    /**
     * Gets query for [[Tienda]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTienda()
    {
        return $this->hasOne(Tienda::className(), ['id' => 'tienda_id']);
    }

    /**
     * Gets query for [[UsuarioCrea]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioCrea()
    {
        return $this->hasOne(User::className(), ['id' => 'usuario_crea']);
    }

    /**
     * Gets query for [[UsuarioModifica]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsuarioModifica()
    {
        return $this->hasOne(User::className(), ['id' => 'usuario_modifica']);
    }
}
