<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tienda}}`.
 */
class m210907_153039_create_tienda_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_spanish_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tienda}}', [
            'id'                => $this->primaryKey(),
            'nombre'            => $this->string()->notNull()->unique(),
            'geolocalizacion'   => $this->string()->notNull()->unique(),
            'fecha_apertura'    => $this->date(),
            'usuario_crea'      => $this->integer()->notNull(),
            'fecha_crea'        => $this->dateTime()->notNull(),
            'usuario_modifica'  => $this->integer()->notNull(),
            'fecha_modifica'    => $this->dateTime()->notNull(),
        ], $tableOptions);

        $this->addForeignKey(
            'usuariocreatienda', 'tienda', 'usuario_crea', 'user', 'id', 'no action', 'no action'
        );
        
        $this->addForeignKey(
            'usuariomodificatienda', 'tienda', 'usuario_modifica', 'user', 'id', 'no action', 'no action'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('usuariocreatienda', 'tienda');
        $this->dropForeignKey('usuariomodificatienda', 'tienda');
        $this->dropTable('{{%tienda}}');
    }
}
