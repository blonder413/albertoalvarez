<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%producto}}`.
 */
class m210907_181127_create_producto_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_spanish_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%producto}}', [
            'id'                => $this->primaryKey(),
            'nombre'            => $this->string()->notNull()->unique(),
            'sku'               => $this->string()->notNull()->unique(),
            'descripcion'       => $this->text(),
            'valor'             => $this->integer()->notNull(),
            'tienda_id'         => $this->integer()->notNull(),
            'recibido'          => $this->boolean()->defaultValue(false),
            'usuario_crea'      => $this->integer()->notNull(),
            'fecha_crea'        => $this->dateTime()->notNull(),
            'usuario_modifica'  => $this->integer()->notNull(),
            'fecha_modifica'    => $this->dateTime()->notNull(),
        ], $tableOptions);

        $this->addForeignKey(
            'usuariocreaproducto', 'producto', 'usuario_crea', 'user', 'id', 'no action', 'no action'
        );
        
        $this->addForeignKey(
            'usuariomodificaproducto', 'producto', 'usuario_modifica', 'user', 'id', 'no action', 'no action'
        );

        $this->addForeignKey(
            'tiendaproducto', 'producto', 'tienda_id', 'tienda', 'id', 'no action', 'no action'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('usuariocreaproducto', 'producto');
        $this->dropForeignKey('usuariomodificaproducto', 'producto');
        $this->dropForeignKey('tiendaproducto', 'producto');
        $this->dropTable('{{%producto}}');
    }
}
