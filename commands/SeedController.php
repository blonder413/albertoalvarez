<?php

// commands/SeedController.php

namespace app\commands;

use app\models\Seguridad;
use yii\console\Controller;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\Expression;
use yii\db\QueryBuilder;
use yii\helpers\Console;
use yii\rbac\DbManager;


class SeedController extends Controller {

    public function actionIndex()
    {
        $faker = \Faker\Factory::create('es_ES');
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $this->actionUser();
            $this->actionTienda();
            $this->actionProducto();

            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    /**
     * Seeder para la tabla user
     */
    public function actionUser()
    {
        $faker = \Faker\Factory::create('es_ES');
        $this->stdout("insertando registros en la tabla user \n", Console::FG_YELLOW);
        
        Yii::$app->db->createCommand()->batchInsert('user',
        [
            'name', 'username', 'auth_key', 'email', 'photo', 'status', 'verification_token',
            'password_hash', 'password_reset_token', 'created_at', 'updated_at'
        ],
        [
            [
                'Jonathan Morales',
                'blonder413',
                $faker->word,
                'blonder413@outlook.com',
                $faker->image('web/img/users', 400, 300, false, true),
                10,
                null,
                Yii::$app->security->generatePasswordHash('123456'),
                null,
                $faker->numberBetween(100000000, 999999999),
                $faker->numberBetween(100000000, 999999999),
            ],
        ]
        )->execute();

        for ($i = 0; $i < 49; $i++) {
            Yii::$app->db->createCommand()->batchInsert('user',
                    [
                        'name', 'username', 'auth_key', 'email', 'photo', 'status', 'verification_token',
                        'password_hash', 'password_reset_token', 'created_at', 'updated_at'
                    ],
                    [
                        [
                            $faker->firstName,
                            $faker->unique()->userName,
                            $faker->word,
                            $faker->freeEmail,
                            //$faker->image('web/img/users', 400, 300, false, true),
                            'user-' . $i . '.jpg',
                            10,
                            null,
                            Yii::$app->security->generatePasswordHash('123456'),
                            null,
                            $faker->numberBetween(100000000, 999999999),
                            $faker->numberBetween(100000000, 999999999),
                        ],
                    ]
            )->execute();
//            $this->stdout("Usuario insertado\n", Console::BOLD);
        }

        $this->stdout("Registros insertados en la tabla user\n", Console::FG_GREEN);
    }

    /**
     * seeder para la tabla tienda
     */
    public function actionTienda()
    {
        $faker = \Faker\Factory::create('es_ES');

        $this->stdout("insertando registros en la tabla tienda\n", Console::FG_YELLOW);
        for ($i=0; $i<100; $i++) {
            Yii::$app->db->createCommand()->batchInsert('tienda',
                [
                    'nombre', 'geolocalizacion', 'fecha_apertura',
                    'usuario_crea', 'fecha_crea', 'usuario_modifica', 'fecha_modifica'
                ],
                [
                    [
                        $faker->company,
                        $faker->unique()->word,
                        $faker->date(),
                        $faker->numberBetween(1,50),
                        new Expression('NOW()'),
                        $faker->numberBetween(1,50),
                        new Expression('NOW()')
                    ],
                ]
            )->execute();
//            $this->stdout("Registro insertado\n", Console::BOLD);
        }

        $this->stdout("Registros insertados en la tabla tienda\n", Console::FG_GREEN);
    }

    /**
     * seeder para la tabla producto
     */
    public function actionProducto()
    {
        $faker = \Faker\Factory::create('es_ES');

        $this->stdout("insertando registros en la tabla producto\n", Console::FG_YELLOW);
        for ($i=0; $i<100; $i++) {
            Yii::$app->db->createCommand()->batchInsert('producto',
                [
                    'nombre', 'sku', 'descripcion', 'valor', 'tienda_id',
                    'usuario_crea', 'fecha_crea', 'usuario_modifica', 'fecha_modifica'
                ],
                [
                    [
                        $faker->unique()->word,
                        $faker->sentence(),
                        $faker->text,
                        $faker->randomNumber(),
                        $faker->numberBetween(1,100),
                        $faker->numberBetween(1,50),
                        new Expression('NOW()'),
                        $faker->numberBetween(1,50),
                        new Expression('NOW()'),
                    ],
                ]
            )->execute();
//            $this->stdout("Registro insertado\n", Console::BOLD);
        }

        $this->stdout("Registros insertados en la tabla producto\n", Console::FG_GREEN);
    }
}
