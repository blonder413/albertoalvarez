<?php

use app\models\Tienda;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Productos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="producto-index box box-primary">
    
    <div class="box-body table-responsive no-padding">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nombre',
            'sku',
            'descripcion:ntext',
            'valor',
            [
                'attribute' => 'tienda_id',
                'value'     => 'tienda.nombre',
                'format'    => 'raw',
                'filter'    => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'tienda_id',
                    'data' => ArrayHelper::map(Tienda::find()->orderBy('nombre asc')->all(), 'id', 'nombre'),
                    'options' => ['placeholder' => 'Seleccione...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]),
            ],
            'recibido:boolean',
            //'usuario_crea',
            //'fecha_crea',
            //'usuario_modifica',
            //'fecha_modifica',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete} {recibir}',
                'buttons' => [
                    'recibir' => function ($url, $model, $key) {
                        if (!$model->recibido) {
                            return Html::a(
                                '<i class="fas fa-check"></i>',
                                $url,
                                [
//                                    'class' => 'btn btn-success',
                                    'title' => Yii::t('app', 'Verificar recibido'),
                                ]
                            );
                        }
                    },
                ]
            ],
        ],
        'pjax'  => false,
        'export'    => [
            'label'     => 'Exportar',
            'messages'  => [
                'confirmDownload'   => 'De acuerdo para proceder',
            ],
//            'showConfirmAlert'  => false,
        ],
        'exportConfig' => [
//            GridView::HTML => [
//            ],
            GridView::CSV => [
            ],
//            GridView::TEXT => [
//            ],
            GridView::EXCEL => [
                'label' => ( 'XLS'),
                'iconOptions' => ['class' => 'text-success'],
                'showHeader' => true,
                'showPageSummary' => true,
                'showFooter' => true,
                'showCaption' => true,
                'filename' => ('archivoDeBlonder413'),
                'alertMsg' => ( 'El archivo de excel se va descargar.'),
                'options' => ['title' => ( 'Excel')],
                'mime' => 'application/vnd.ms-excel',
                'config' => [
                    'worksheet' => ( 'ExportWorksheet'),
                    'cssFile' => '',
                ]
            ],
            GridView::PDF => [
            ],
//            GridView::JSON => [
//            ],
        ],
        'hover'         => true,
        'toolbar' => [
            '{toggleData}',
            '{export}',
        ],
        'toggleDataContainer' => ['class' => 'btn-group mr-2'],
        'responsive'    => true,
        'panel'     => [
            'after'=>Html::a('<i class="fas fa-redo"></i> Limpiar Tabla', ['index'], ['class' => 'btn btn-info']),
            'before'=>Html::a('<i class="glyphicon glyphicon-plus"></i> Crear', ['create'], ['class' => 'btn btn-success']),
            'heading' => 'Administrar Producto',
            'type'  => GridView::TYPE_SUCCESS,
            
            
        ],
    ]); ?>


</div>
