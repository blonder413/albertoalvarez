RREQUISITOS
------------

Los requisitos mínimos requeridos para este proyecto es un servidor web con soporte para PHP 5.6.0.


INSTALACION
------------

### Install via git

Si no tiene [Git](https://git-scm.com/), puede descargarlo
en [git-scm.com](https://git-scm.com/downloads).

Primero debe ubicarse en la carpeta raíz de su servidor web desde la consola de comandos.
Una vez allí puede instalar el proyecto usando el siguiente comando:

~~~
git clone https://gitlab.com/blonder413/albertoalvarez
~~~

### Instalar desde un archivo

Extraiga el archivo descargado desde [git](https://gitlab.com/blonder413/albertoalvarez) 
al directorio raiz de su servidor web.


### Instalar extensiones

Si no tiene [Composer](http://getcomposer.org/), puede instalarlo siguiendo las instrucciones
en [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

Una vez que tenga composer debe dirigirse a la carpeta del proyecto e instalar las extensiones

~~~
cd albertoalvarez
composer install
~~~

CONFIGURACIÓN
-------------

### Base de datos

Renombre el archivo `config/db_ejemplo.php` como `config/db.php` y modifíquelo con información real, por ejemplo:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=albertoalvarez',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

Renombre el archivo `config/mailer_ejemplo.php` como `config/mailer.php` y modifíquelo con información de un correo electrónico real.
Esta información es necesaria para el envío de correos tanto en la página de contacto como para registrar usuarios y recuperar contraseñas.

Renombre el archivo `config/params_ejemplo.php` como `config/params.php` y modifíquelo con información de correos electrónicos reales.
Esta información se mostrará al enviar correos electrónicos.

Una vez configurada la conexión a la base de datos, podemos proceder a ejecutar las migraciones, esto creará las tablas necesarias.

```
php yii migrate
```

Si desea puede insertar datos de prueba ejecutando el siguiente comando.

```
php yii seed
```

Este comando creará una serie de registros, entre ellos varios usuarios con la contraseña `123456`
(los nombres de usuario puede consultarlos en la base de datos ya que son aleatorios)

Si está usando GNU/Linux es probable que necesite darle permisos de lectura y escritura a los directorios `runtime` y `web`.

Una vez hecho esto tendrá acceso a la aplicación a través de la siguiente URL:

~~~
http://localhost/albertoalvarez/web/
~~~